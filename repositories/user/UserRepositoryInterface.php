<?php

namespace app\repositories\user;

interface UserRepositoryInterface
{

    public function getUsersForList(): array;

    public function getUsersForNotify(int $exceptAuthorId): array;

}