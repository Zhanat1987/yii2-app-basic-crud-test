<?php

namespace app\repositories\user;

use app\models\User;

class UserRepository implements UserRepositoryInterface
{

    public function getUsersForList(): array
    {
        $res = [];
        $users = User::find()
            ->select('id, username')
            ->asArray()
            ->all();
        if ($users) {
            foreach ($users as $user) {
                $res[$user['id']] = $user['username'];
            }
        }

        return $res;
    }

    public function getUsersForNotify(int $exceptAuthorId): array
    {
        return User::find()
            ->select('email, username')
            ->where('id != :id', ['id' => $exceptAuthorId])
            ->asArray()
            ->all();
    }

}