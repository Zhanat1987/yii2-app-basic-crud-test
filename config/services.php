<?php

use app\repositories\user\{
    UserRepositoryInterface,
    UserRepository
};

return [
    // Container itself
    Psr\Container\ContainerInterface::class => function (\DI\Container $container) {
        return $container;
    },

    // Services
    UserRepositoryInterface::class => DI\object(UserRepository::class),
];