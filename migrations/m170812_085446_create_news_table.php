<?php

use yii\db\Migration;

/**
 * Handles the creation of table `news`.
 */
class m170812_085446_create_news_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('news', [
            'id' => $this->primaryKey(),
            'title' => $this->string()->unique()->notNull(),
            'short_text' => $this->text()->notNull(),
            'text' => $this->text()->notNull(),
            'img' => $this->string(),
            'author_id' => $this->integer()->notNull(),
            'updater_id' => $this->integer(),
            'status' => $this->smallInteger(1)->defaultValue(1),
            'created_at' => $this->timestamp()->defaultExpression('NOW()'),
            'views' => $this->integer(),
        ]);

        $this->addForeignKey('news_author_fk', 'news', 'author_id', 'user', 'id');
        $this->addForeignKey('news_updater_fk', 'news', 'updater_id', 'user', 'id');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('news');
    }
}
