<?php
use yii\helpers\Url as Url;

class LoginCest
{
    public function ensureThatLoginWorks(AcceptanceTester $I)
    {
        $I->amOnPage(Url::toRoute('/user/security/login'));
        $I->see('Sign in', 'h3');

        $I->fillField('input[name="login-form[login]"]', 'admin');
        $I->fillField('input[name="login-form[password]"]', 'testtest');
        $I->click('#login-form button[type=submit]');
        $I->wait(2); // wait for button to be clicked

        $I->expectTo('see news list');
        $I->see('News', 'h1');
    }
}
