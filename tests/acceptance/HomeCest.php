<?php
use yii\helpers\Url as Url;

class HomeCest
{
    public function ensureThatHomePageWorks(AcceptanceTester $I)
    {
        $I->amOnPage(Url::toRoute('/news/news/index'));
        $I->see('News');
        
        $I->seeLink('Login');
        $I->click('Login');
        $I->wait(2); // wait for page to be opened

        $I->see('Sign in');
    }
}
