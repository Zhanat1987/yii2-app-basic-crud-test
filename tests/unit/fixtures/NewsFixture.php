<?php

namespace app\tests\unit\fixtures;

use app\modules\news\models\News;
use yii\test\ActiveFixture;

class NewsFixture extends ActiveFixture
{
    /**
     * {@inheritdoc}
     */
    public $modelClass = News::class;
    public $dataFile = __DIR__ . '/data/news.php';

}
