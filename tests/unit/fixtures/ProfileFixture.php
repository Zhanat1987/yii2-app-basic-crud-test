<?php

namespace app\tests\unit\fixtures;

use dektrium\user\models\Profile;
use yii\test\ActiveFixture;

class ProfileFixture extends ActiveFixture
{
    /**
     * {@inheritdoc}
     */
    public $modelClass = Profile::class;

    public $dataFile = __DIR__ . '/data/profile.php';

    public $depends = [UserFixture::class];

}
