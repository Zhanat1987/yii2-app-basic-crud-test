<?php

namespace app\tests\unit\fixtures;

use dektrium\user\models\User;
use yii\test\ActiveFixture;

class UserFixture extends ActiveFixture
{
    /**
     * {@inheritdoc}
     */
    public $modelClass = User::class;
    public $dataFile = __DIR__ . '/data/user.php';

}
