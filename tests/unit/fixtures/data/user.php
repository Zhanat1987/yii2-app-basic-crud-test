<?php

$time = time();

return [
    [
        'id' => 1,
        'username' => 'admin2',
        'auth_key' => 'admin_auth_key',
        'password_hash' => '$2y$10$ECZmesjvOORgWHCxKVoTTuaK2NauwqVg4cacpmIr1xVd.oFxtgNRK', // testtest
        'email' => 'admin@test.com',
        'created_at' => $time,
        'updated_at' => $time,
        'confirmed_at' => $time,
    ],
    [
        'id' => 2,
        'username' => 'moder',
        'auth_key' => 'moder_auth_key',
        'password_hash' => '$2y$10$ECZmesjvOORgWHCxKVoTTuaK2NauwqVg4cacpmIr1xVd.oFxtgNRK',
        'email' => 'moder@test.com',
        'created_at' => $time,
        'updated_at' => $time,
        'confirmed_at' => $time,
    ],
    [
        'id' => 3,
        'username' => 'user',
        'auth_key' => 'user_auth_key',
        'password_hash' => '$2y$10$ECZmesjvOORgWHCxKVoTTuaK2NauwqVg4cacpmIr1xVd.oFxtgNRK',
        'email' => 'user@test.com',
        'created_at' => $time,
        'updated_at' => $time,
        'confirmed_at' => $time,
    ],
];
