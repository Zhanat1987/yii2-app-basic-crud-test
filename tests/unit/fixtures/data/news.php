<?php

$faker = Faker\Factory::create();

return [
    [
        'id' => 1,
        'title' => join(' ', $faker->words),
        'short_text' => $faker->paragraph(),
        'text' => $faker->paragraph(10),
        'img' => '03cbc1a9f9178055093eb0c25ba9df2c29611671.png.jpeg',
        'author_id' => mt_rand(1, 3),
    ],
    [
        'id' => 2,
        'title' => join(' ', $faker->words),
        'short_text' => $faker->paragraph(),
        'text' => $faker->paragraph(10),
        'img' => 'go_lang1.png',
        'author_id' => mt_rand(1, 3),
    ],
    [
        'id' => 3,
        'title' => join(' ', $faker->words),
        'short_text' => $faker->paragraph(),
        'text' => $faker->paragraph(10),
        'img' => 'golang images.jpeg',
        'author_id' => mt_rand(1, 3),
    ],
    [
        'id' => 4,
        'title' => join(' ', $faker->words),
        'short_text' => $faker->paragraph(),
        'text' => $faker->paragraph(10),
        'img' => 'Gopher_GoRequest_400x300.jpg',
        'author_id' => mt_rand(1, 3),
    ],
    [
        'id' => 5,
        'title' => join(' ', $faker->words),
        'short_text' => $faker->paragraph(),
        'text' => $faker->paragraph(10),
        'img' => 'images.jpeg',
        'author_id' => mt_rand(1, 3),
    ],
];
