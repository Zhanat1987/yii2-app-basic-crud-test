<?php

namespace tests\models;

use app\modules\news\models\News;
use yii\codeception\DbTestCase;
use app\tests\unit\fixtures\NewsFixture;

class NewsTest extends DbTestCase
{

    public function fixtures()
    {
        return [
            'news' => NewsFixture::className(),
        ];
    }

    public function testFindNewsById()
    {
        expect_that($news = News::findOne(1));
        expect($news->img)->equals('03cbc1a9f9178055093eb0c25ba9df2c29611671.png.jpeg');
        expect_not(News::findOne(999));
    }

}
