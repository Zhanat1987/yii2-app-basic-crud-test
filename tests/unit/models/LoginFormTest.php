<?php

namespace tests\models;

use dektrium\user\models\LoginForm;

class LoginFormTest extends \Codeception\Test\Unit
{
    /**
     * @var LoginForm
     */
    private $model;

    protected function _after()
    {
        \Yii::$app->user->logout();
    }

    public function testLoginNoUser()
    {
        $this->createLoginForm([
            'login' => 'not_existing_username',
            'password' => 'not_existing_password',
        ]);
        expect_not($this->model->login());
        expect_that(\Yii::$app->user->isGuest);
    }

    public function testLoginWrongPassword()
    {
        $this->createLoginForm([
            'login' => 'user',
            'password' => 'wrong_password',
        ]);
        expect_not($this->model->login());
        expect_that(\Yii::$app->user->isGuest);
        expect($this->model->errors)->hasKey('password');
    }

    public function testLoginCorrect()
    {
        $this->createLoginForm([
            'login' => 'user',
            'password' => 'testtest',
        ]);
        expect_that($this->model->login());
        expect_not(\Yii::$app->user->isGuest);
        expect($this->model->errors)->hasntKey('password');
    }

    private function createLoginForm(array $credentials)
    {
        $this->model = \Yii::createObject(LoginForm::className());
        $this->model->login = $credentials['login'];
        $this->model->password = $credentials['password'];
    }
}