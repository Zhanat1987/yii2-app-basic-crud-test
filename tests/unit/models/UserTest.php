<?php

namespace tests\models;

use app\models\User;

class UserTest extends \Codeception\Test\Unit
{

    public function testFindUserById()
    {
        expect_that($user = User::findIdentity(1));
        expect($user->username)->equals('admin');
        expect_not(User::findIdentity(999));
    }

    /**
     * @expectedException \yii\base\NotSupportedException
     */
    public function testFindUserByAccessToken()
    {
        expect_that($user = User::findIdentityByAccessToken('token-value'));
    }

    public function testFindUserByUsername()
    {
        expect_that($user = User::find()->where('username = :username', ['username' => 'admin'])->one());
        expect_not(User::find()->where('username = :username', ['username' => 'not-admin'])->one());
    }

    /**
     * @depends testFindUserByUsername
     */
    public function testValidateUser($user)
    {
        $user = User::findIdentity(1);
        expect_that($user->validateAuthKey('admin_auth_key'));
        expect_not($user->validateAuthKey('test102key'));
    }

    /**
     * @dataProvider nonExistingIdsDataProvider
     */
    public function testFindIdentityReturnsNullIfUserIsNotFound($invalidId)
    {
        $this->assertNull(User::findIdentity($invalidId));
    }

    public function nonExistingIdsDataProvider()
    {
        return [[-1], [null], [30]];
    }
}
