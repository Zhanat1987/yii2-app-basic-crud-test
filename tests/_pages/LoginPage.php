<?php

namespace app\tests\_pages;

use yii\codeception\BasePage;

/**
 * Represents login page
 * @property \AcceptanceTester|\FunctionalTester $actor
 */
class LoginPage extends BasePage
{

    public $route = 'user/security/login';

    /**
     * @param string $username
     * @param string $password
     */
    public function login($username, $password)
    {
        $this->actor->amOnPage($this->route);
        $this->actor->fillField(['name' => 'login-form[login]'], $username);
        $this->actor->fillField(['name' => 'login-form[password]'], $password);
        $this->actor->click('#login-form button[type=submit]');
    }
}
