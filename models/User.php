<?php

namespace app\models;

class User extends \dektrium\user\models\User
{

    public function getIsAdmin(): bool
    {
        return \Yii::$app->user->can('admin');
    }

}
