<?php

namespace app\modules\news\models;

use app\models\User;
use app\modules\news\services\NotifyManager;
use yii\behaviors\BlameableBehavior;
use yii\web\UploadedFile;

/**
 * This is the model class for table "news".
 *
 * @property int $id
 * @property string $title
 * @property string $short_text
 * @property string $text
 * @property string $img
 * @property int $author_id
 * @property int $updater_id
 * @property int $status
 * @property string $created_at
 * @property int $views
 *
 * @property User $author
 * @property User $updater
 */
class News extends \yii\db\ActiveRecord
{

    const BOOL_TRUE  = 1;
    const BOOL_FALSE = 0;

    /**
     * @var UploadedFile
     */
    public $imageFile;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'news';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'short_text', 'text'], 'required'],
            [['text'], 'string'],
            [['author_id', 'views'], 'integer'],
            ['status', 'in', 'range' => [self::BOOL_FALSE, self::BOOL_TRUE, 'Yes', 'No']],
            [['created_at'], 'safe'],
            [['title', 'img'], 'string', 'max' => 255],
            [['title'], 'unique'],
            [['author_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['author_id' => 'id']],
            [
                'imageFile',
                'image',
                'extensions' => 'png, jpg, jpeg',
                'minWidth' => 100,
                'maxWidth' => 2000,
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'short_text' => 'Short text',
            'text' => 'Text',
            'imageFile' => 'Image',
            'img' => 'Img',
            'author_id' => 'Author',
            'updater_id' => 'Updater',
            'status' => 'Status',
            'created_at' => 'Created At',
            'views' => 'Views',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(User::className(), ['id' => 'author_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdater()
    {
        return $this->hasOne(User::className(), ['id' => 'updater_id']);
    }

    public function behaviors()
    {
        return [
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'author_id',
                'updatedByAttribute' => 'updater_id',
            ]
        ];
    }

    public function getStatuses(): array
    {
        return [
            self::BOOL_FALSE => 'No',
            self::BOOL_TRUE => 'Yes',
        ];
    }

    public function beforeSave($insert): bool
    {
        $this->imageFile = UploadedFile::getInstance($this, 'imageFile');
        if (!empty($this->imageFile)) {
            if ($this->img) {
                @unlink($this->getImagePath());
            }
            $this->img = \Yii::$app->security->generateRandomString() . '.' . $this->imageFile->extension;
            $this->imageFile->saveAs('img/news/' . $this->img);
        }

        if (!$insert) {
            $this->status = is_numeric($this->status) ? $this->status : ['No' => 0, 'Yes' => 1][$this->status];
        }

        return parent::beforeSave($insert);
    }

    public function afterSave($insert, $changedAttributes): void
    {
        parent::afterSave($insert, $changedAttributes);

        if ($insert) {
            (new NotifyManager())->send($this);
        }
    }

    public function getImagePath(): string
    {
        return \Yii::getAlias('@app') . '/web/img/news/' . $this->img;
    }

    public function incViews(): void
    {
        $this->views++;
        $this->save(false);
    }
}
