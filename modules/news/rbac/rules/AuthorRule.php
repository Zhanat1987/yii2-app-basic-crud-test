<?php

namespace app\modules\news\rbac\rules;

use yii\rbac\Rule;

class AuthorRule extends Rule
{

    public function execute($user, $item, $params): bool
    {
        return \Yii::$app->user->can('admin') ||
            (isset($params['news']) ? $params['news']->author_id == $user : false);
    }

}