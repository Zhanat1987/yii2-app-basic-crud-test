<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\news\models\News */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'News', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="news-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            Yii::$app->user->isGuest ? 'short_text:ntext' : 'text:ntext',
            [
                'attribute' => 'img',
                'format' => 'html',
                'value' => function ($data) {
                    return isset($data['img']) ? Html::img('/img/news/' . $data['img'],
                        ['width' => '60px']) : null;
                },
            ],
            [
                'attribute' => 'author_id',
                'value' => function ($model) {
                    return $model->author->username ?? null;
                },
            ],
            [
                'attribute' => 'status',
                'format' => 'html',
                'value' => function ($model) {
                    return $model->status ? '<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>' :
                        '<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>';
                },
            ],
            'created_at',
            'views',
        ],
    ]) ?>

</div>
