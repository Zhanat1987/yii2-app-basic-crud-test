<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use jino5577\daterangepicker\DateRangePicker;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\news\models\NewsSearch */
/* @var $model app\modules\news\models\News */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $user yii\web\User */
/* @var $users array */

$this->title = 'News';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="news-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(['id' => 'news-grid-container-id']) ?>

    <?php if ($user->can('moder')) : ?>
        <p>
            <?= Html::a('Create News', ['news/create'], ['class' => 'btn btn-success modalButton']) ?>
        </p>
    <?php endif; ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'title',
            'short_text:ntext',
            [
                'attribute' => 'img',
                'format' => 'html',
                'value' => function ($data) {
                    return isset($data['img']) ? Html::img('/img/news/' . $data['img'],
                        ['width' => '60px']) : null;
                },
            ],
            [
                'attribute' => 'author_id',
                'format' => 'html',
                'value' => function ($model) {
                    return $model->author->username ?? null;
                },
                'filter' => $users,
            ],
            $user->can('moder') ?
                [
                    'attribute' => 'status',
                    'format' => 'html',
                    'value' => function ($model) {
                        return $model->status ? '<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>' :
                            '<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>';
                    },
                    'filter' => $searchModel->getStatuses(),
                    'class' => \yii2mod\editable\EditableColumn::className(),
                    'url' => ['/news/news/change-status'],
                    'type' => 'select',
                    'editableOptions' => function ($model) {
                        return [
                            'source' => [
                                'No' => 'No',
                                'Yes' => 'Yes',
                            ],
                            'value' => $model->status ? 'Yes' : 'No',
                        ];
                    },
                ]
                :
                [
                    'attribute' => 'status',
                    'format' => 'html',
                    'value' => function ($model) {
                        return $model->status ? '<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>' :
                            '<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>';
                    },
                    'filter' => $searchModel->getStatuses(),
                ],
            [
                'attribute' => 'created_at',
                'headerOptions' => [
                    'class' => 'col-md-2'
                ],
                'filter' => DateRangePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'created_at_range',
                    'pluginOptions' => [
                        'format' => 'd-m-Y',
                        'autoUpdateInput' => false
                    ]
                ])
            ],
            'views',
            [
                'class' => 'yii\grid\ActionColumn',
                'visibleButtons' => [
                    'update' => function ($model, $key, $index) use ($user) {
                        return $user->can('admin') || $user->can('updateNews', ['news' => $model]);
                    },
                    'delete' => function ($model, $key, $index) use ($user) {
                        return $user->can('admin') || $user->can('deleteNews', ['news' => $model]);
                    },
                ],
                'template' => '{view} {update} {delete}',
                'buttons' =>[
                    'update' => function ($url, $model) {
                        return Html::a( '<span class="glyphicon glyphicon-pencil"></span>',
                            ['news/update', 'id' => $model->id],
                            ['class'=>'btn btn-xs btn-default modalButton', 'title' => 'update news']
                        );
                    },
                ]

            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
<?php
// JS: MODAL EDIT & PJAX BELOW.
yii\bootstrap\Modal::begin([
    'header' => 'News',
    'id'=>'editModalId',
    'class' =>'modal',
    'size' => 'modal-md',
]);
echo "<div class='modalContent'></div>";
yii\bootstrap\Modal::end();

$this->registerJs(
"$(document).on('ready pjax:success', function() {
    $('.modalButton').click(function(e){
        e.preventDefault();
        var tagname = $(this)[0].tagName;
        $('#editModalId').modal('show').find('.modalContent').load($(this).attr('href'));
    });
});
"
);

// JS: Update response handling
$this->registerJs(
'jQuery(document).ready(function($){
    $(document).ready(function () {
        $("body").on("beforeSubmit", "form#newsForm", function () {
            var form = $(this);
            // return false if form still have some validation errors
            if (form.find(".has-error").length) {
                return false;
            }
            
            // submit form
            var formData = new FormData(form[0]);
            $.ajax({
                url    : form.attr("action"),
                type   : "post",
                data   : formData,
                success: function (response) {
                    $("#editModalId").modal("toggle");
                    $.pjax.reload({container: "#news-grid-container-id"}); // for pjax update
                },
                error  : function () {
                    console.log("internal server error");
                },
                cache: false,
                contentType: false,
                processData: false
            });
            
            return false;
         });
    });
});'
);
?>