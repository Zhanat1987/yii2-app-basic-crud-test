<?php

namespace app\modules\news\services;

use app\modules\news\jobs\NotifyNewsJob;
use app\modules\news\models\News;
use Yii;

class NotifyManager
{

    public function send(News $news): void
    {
        Yii::$app->queue->push(new NotifyNewsJob([
            'news' => $news,
        ]));
    }

}