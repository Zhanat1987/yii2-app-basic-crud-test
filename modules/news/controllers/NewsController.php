<?php

namespace app\modules\news\controllers;

use app\controllers\BaseController;
use app\repositories\user\UserRepositoryInterface;
use Yii;
use app\modules\news\models\News;
use app\modules\news\models\NewsSearch;
use yii\filters\AccessControl;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii2mod\editable\EditableAction;

/**
 * NewsController implements the CRUD actions for News model.
 */
class NewsController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['create', 'update', 'delete', 'change-status'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['create'],
                        'roles' => ['createNews'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['update'],
                        'roles' => ['updateNews'],
                        'roleParams' => function() {
                            return ['news' => News::findOne(Yii::$app->request->get('id'))];
                        },
                    ],
                    [
                        'allow' => true,
                        'actions' => ['delete'],
                        'roles' => ['deleteNews'],
                        'roleParams' => function() {
                            return ['news' => News::findOne(Yii::$app->request->get('id'))];
                        },
                    ],
                    [
                        'allow' => true,
                        'actions' => ['change-status'],
                        'roles' => ['updateNews'],
                        'roleParams' => function() {
                            $id = unserialize(base64_decode(Yii::$app->request->post('pk')));
                            return ['news' => News::findOne($id)];
                        },
                    ],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'change-status' => [
                'class' => EditableAction::className(),
                'modelClass' => News::className(),
                'forceCreate' => false
            ]
        ];
    }

    /**
     * Lists all News models.
     *
     * @param UserRepositoryInterface $userRepository
     *
     * @return mixed
     */
    public function actionIndex(UserRepositoryInterface $userRepository)
    {
        $searchModel = new NewsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $user = Yii::$app->user;
        $users = $userRepository->getUsersForList();

        return $this->render('index', compact('searchModel', 'dataProvider', 'user', 'users'));
    }

    /**
     * Displays a single News model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $model->incViews();
        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * @return array|string
     * @throws HttpException
     */
    public function actionCreate()
    {
        $model = new News();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            \Yii::$app->response->format = Response::FORMAT_JSON;
            return ['model' => $model];
        } else if (\Yii::$app->request->isAjax) {
            return $this->renderAjax('_form', [
                'model' => $model
            ]);
        } else {
            throw new HttpException(405, 'need post or ajax request');
        }
    }

    /**
     * @param $id
     * @return array|string
     * @throws HttpException
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            \Yii::$app->response->format = Response::FORMAT_JSON;
            return ['model' => $model];
        } else if (\Yii::$app->request->isAjax) {
            return $this->renderAjax('_form', [
                'model' => $model
            ]);
        } else {
            throw new HttpException(405, 'need post or ajax request');
        }
    }

    /**
     * Deletes an existing News model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the News model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return News the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = News::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
