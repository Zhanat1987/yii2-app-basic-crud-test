<?php

namespace app\modules\news\jobs;

use app\jobs\SendEmailJob;
use app\modules\news\models\News;
use app\repositories\user\UserRepository;
use yii\base\BaseObject;
use yii\queue\Job;
use Yii;

class NotifyNewsJob extends BaseObject implements Job
{

    /**
     * @var News
     */
    public $news;

    public function execute($queue): void
    {
        $users = (new UserRepository())->getUsersForNotify($this->news->author_id);
        if ($users) {
            $msg = <<<MSG
Hi, %s!<br />
We have posted a new news on site:<br />
<a href="%s">%s</a>
MSG;
            foreach ($users as $user) {
                $text = sprintf($msg, $user['username'],
                    Yii::$app->urlManager->createAbsoluteUrl(['news/news/view', 'id' => $this->news->id]), $this->news->title);
                Yii::$app->queue->push(new SendEmailJob([
                    'from'    => Yii::$app->params['adminEmail'],
                    'to'      => $user['email'],
                    'subject' => 'New news on our site',
                    'text'    => $text,
                    'isHtml'  => true,
                ]));
            }
        }
    }
}
