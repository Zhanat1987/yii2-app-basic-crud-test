<?php

namespace app\widgets\menu;

use Yii;
use yii\helpers\Html;

class Menu extends \yii\bootstrap\Widget
{

    /**
     * {@inheritdoc}
     */
    public function run()
    {
        $items = [
            ['label' => 'Home', 'url' => ['/news/news/index']],
        ];
        $user = Yii::$app->user;
        if ($user->isGuest) {
            $items[] = ['label' => 'Login', 'url' => ['/user/security/login']];
            $items[] = ['label' => 'Sign up', 'url' => ['/user/registration/register']];
        } else {
            $identity = $user->identity;
            $items[] = '<li>'
                . Html::beginForm(['/user/security/logout'], 'post')
                . Html::submitButton(
                    'Logout (' . $identity->username . ')',
                    ['class' => 'btn btn-link logout']
                )
                . Html::endForm()
                . '</li>';
            $items[] = ['label' => 'Profile', 'url' => ['/user/profile/index']];
            $items[] = [
                'label' => 'Admin dashboard',
                'url' => ['/user/admin/index'],
                'visible' => $user->can('adminDashboard')
            ];
        }

        return $this->render('menu', ['items' => $items]);
    }
}
