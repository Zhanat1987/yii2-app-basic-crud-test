<?php

namespace app\controllers;

use app\traits\WebControllerAutowiring;
use Yii;
use yii\web\Controller;

class BaseController extends Controller
{

    use WebControllerAutowiring;

}
