Simple blog based on Yii 2 Basic Project Template
-------------------------------------------------


DIRECTORY STRUCTURE
-------------------

      assets/             contains assets definition
      commands/           contains console commands (controllers)
      config/             contains application configurations
      controllers/        contains Web controller classes
      docker/             contains files for run docker containers
      mail/               contains view files for e-mails
      models/             contains model classes
      migrations/         contains database migrations
      runtime/            contains files generated during runtime
      tests/              contains various tests for the basic application
      vendor/             contains dependent 3rd-party packages
      views/              contains view files for the Web application
      web/                contains the entry script and Web resources



REQUIREMENTS
------------

git, docker, docker-compose


INSTALLATION
------------

### Install

```
git clone https://bitbucket.org/Zhanat1987/yii2-app-basic-crud-test.git && \
cd yii2-app-basic-crud-test && composer install && \
cd docker && cp .env.dist .env && \
docker-compose up -d && docker-compose exec php bash
```

### Run inside docker container and stay in terminal for queues works

```
php yii migrate/up --migrationPath=@vendor/dektrium/yii2-user/migrations && \
php yii migrate/up --migrationPath=@yii/rbac/migrations && \
php yii migrate && php yii fixture/load "*" && \
php yii rbac-init && \
chmod -R 0777 web/assets runtime web/img && \
php yii queue/listen
```

```
http://localhost/
http://localhost/user/login - admin/testtest | moder/testtest | user/testtest
```

### Run tests inside docker container

```
tests/bin/yii migrate/up --migrationPath=@vendor/dektrium/yii2-user/migrations && \
tests/bin/yii migrate/up --migrationPath=@yii/rbac/migrations && \
tests/bin/yii migrate && tests/bin/yii fixture/load "*" && \
tests/bin/yii rbac-init
```


NOTIFICATIONS
-------------

```
Для реализации нотификаций клиентов на фронте я бы использовал готовый пакет:
https://packagist.org/packages/machour/yii2-notifications
https://machour.idk.tn/yii/machour/yii2-notifications/Installation.md
Настроил бы его для необходимых типов уведомлений и переопределил бы класс
https://machour.idk.tn/yii/machour/yii2-notifications/Configuration.md
machour\yii2\notifications\models\Notification 
для уведомлений о новых пользователях

Для email нотификаций я бы добавил таблицы:
notification:
id
title (новость новая, новый пользователь, etc)

user_notification:
notification_id
user_id

Затем бы создал behavior, который бы подписывался на события 
в нужных сущностях и отправлял уведомления
всем пользователям подписанным на нотификацию данную.

Была бы инъекция зависимости ввиде интерфейса в поведение, 
и там был бы метод send(Notification $notification): bool
И уже были бы конкретные реализации: 
с помощью e-mail, telegram, sms, etc
```

### Resume

https://hh.kz/resume/8e1b40caff03f8da3f0039ed1f436539367a5a