# How to customise this stack

* [Add PHPMyAdmin](#Add-phpmyadmin)
* [Add Redis](#Add-redis)

## Add PHPMyAdmin

1. Update docker-compose.yml file and add the following lines:

    ```yml
    service:
        # ...
        phpmyadmin:
            image: phpmyadmin/phpmyadmin
            ports:
                - "8080:80"
    ```

2. Visit: [yii.dev:8080](http://yii.dev:8080)

## Add Redis

1. Update docker-compose.yml file and add the following lines:

    ```yml
    service:
        # ...
        redis:
            image: redis:alpine
            ports:
                - 6379:6379
    ```

2. Adapt your Yii configuration file

    ```yml
    # path/to/your/yii-project/app/config/parameters.yml
    parameters:
        #...
        redis_host: redis
    ```

:question: Using [SncRedis](https://github.com/snc/SncRedisBundle)?  
Your Yii config file should be like this:

```yml
snc_redis:
    clients:
        default:
            type: predis
            alias: default
            dsn: redis://%redis_host%
```

Access to redis-cli with:

```bash
# Redis commands
$ docker-compose exec redis redis-cli
```
