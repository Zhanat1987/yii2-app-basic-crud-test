<?php

namespace app\commands;

use app\modules\news\rbac\rules\AuthorRule;
use Yii;
use yii\console\Controller;

/**
 * Инициализатор RBAC выполняется в консоли php yii rbac-init
 */
class RbacInitController extends Controller
{

    public function actionIndex()
    {
        $auth = Yii::$app->authManager;

        $auth->removeAll(); //На всякий случай удаляем старые данные из БД...

        // Создадим роли админа и редактора новостей
        $admin = $auth->createRole('admin');
        $moder = $auth->createRole('moder');
        $user = $auth->createRole('user');

        // запишем их в БД
        $auth->add($admin);
        $auth->add($moder);
        $auth->add($user);

        $adminDashboard = $auth->createPermission('adminDashboard');
        $adminDashboard->description = 'admin dashboard';

        $createNews = $auth->createPermission('createNews');
        $createNews->description = 'Create news';

        $updateNews = $auth->createPermission('updateNews');
        $updateNews->description = 'Edit news';
        $updateNews->ruleName = AuthorRule::class;

        $deleteNews = $auth->createPermission('deleteNews');
        $deleteNews->description = 'Delete news';
        $deleteNews->ruleName = AuthorRule::class;

        $auth->add($adminDashboard);
        $auth->add($createNews);
        $auth->add($updateNews);
        $auth->add($deleteNews);

        $auth->addChild($moder, $createNews);
        $auth->addChild($moder, $updateNews);
        $auth->addChild($moder, $deleteNews);

        $auth->addChild($admin, $moder);

        $auth->addChild($admin, $adminDashboard);

        $auth->assign($admin, 1);
        $auth->assign($moder, 2);
        $auth->assign($user, 3);

        $this->stdout("rbac success init\r\n");
    }
}
