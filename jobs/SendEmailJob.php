<?php

namespace app\jobs;

use yii\base\BaseObject;
use yii\queue\Job;
use Yii;

class SendEmailJob extends BaseObject implements Job
{

    public $from, $to, $subject, $text, $isHtml;

    public function execute($queue): void
    {
        $msg = Yii::$app->mailer->compose()
            ->setFrom($this->from)
            ->setTo($this->to)
            ->setSubject($this->subject);
        $msg = $this->isHtml ? $msg->setHtmlBody($this->text) :  $msg->setTextBody($this->text);
        $msg->send();
    }
}
