<?php

namespace app\components;

use DI\Container;
use yii\base\Component;

class DiWrapper extends Component
{
    /**
     * @var string
     */
    public $config;

    /**
     * @var Container
     */
    private $container;

    /**
     * All methods call are redirected to container instance.
     *
     * @param string $name
     * @param array  $params
     *
     * @return mixed
     */
    public function __call($name, $params)
    {
        return call_user_func_array([$this->container, $name], $params);
    }

    public function init()
    {
        $builder = new \DI\ContainerBuilder();
        $builder->addDefinitions($this->config);
        $this->container = $builder->build();
    }
}
